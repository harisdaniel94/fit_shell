/**
 * project: simple shell
 *    file: shell.h
 *  author: Daniel Haris (xharis00)
 *    date: 09.04.2017
 */

#ifndef FIT_SHELL_SHELL_H
#define FIT_SHELL_SHELL_H
#endif //FIT_SHELL_SHELL_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>

#define ESTIMATED_ARGV_SIZE 20
#define NEWLINE_SYMBOL '\n'
#define TRAILING_ZERO_SYMBOL '\0'
#define REDIRECT_STDOUT_SYMBOL '>'
#define REDIRECT_STDIN_SYMBOL '<'
#define BACKGROUND_PROCESS_SYMBOL '&'
#define DELIMITERS " \t\r\n\v\f"
#define MAX_BUFFER_SIZE 513

enum cmdFlagCodes {
    REDIRECT_STDOUT_FLAG = 0,
    REDIRECT_STDIN_FLAG,
    BACKGROUND_PROCESS_FLAG
};

enum msgCodes {
    MSG_NEWLINE = 0,
    MSG_NEW_PROMPT_LINE,
    MSG_TOO_LONG_INPUT,
    MSG_INVALID_INPUT,
    MSG_FORK_ERROR,
    MSG_FORK_WAIT_ERROR,
    MSG_OPEN_ERROR,
    MSG_THREAD_CREATE_ERROR,
    MSG_THREAD_JOIN_ERROR
};

static void sigchildHandler(int);
static void sigintHandler(int);
static void sigintShellHandler(int);
void printAndFlush(int);
int findCharInString(char, char *);
void *readRoutine(void *);
void *runRoutine(void *);
char **parseArguments(void);
void setCmdFlags(char *);
void clearFlags(void);
char *getFilePath(char, char **);
void removeBackgroundProcessSymbol(char **);
void redirect(char, char *);
void ignoreSignals(void);
