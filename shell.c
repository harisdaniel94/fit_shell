/**
 * project: simple shell
 *    file: shell.c
 *  author: Daniel Haris (xharis00)
 *    date: 09.04.2017
 */

#include "shell.h"

// define global variables
pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
char buffer[MAX_BUFFER_SIZE];
bool exitShell = false;
bool tooLongInput = false;
bool isReading = false;
pid_t childPid = 0;
char *outputFileName = NULL;
char *inputFileName = NULL;

// command flags
bool cmdFlags[] = {
    false,  // REDIRECT_STDOUT_FLAG
    false,  // REDIRECT_STDIN_FLAG
    false   // BACKGROUND_PROCESS_FLAG
};

// messages
const char *msgs[] = {
    // MSG_NEWLINE
    "\n",
    // MSG_NEW_PROMPT_LINE
    "$ ",
    // MSG_TOO_LONG_INPUT
    "Input too long\n",
    // MSG_INVALID_INPUT
    "Invalid input\n",
    // MSG_FORK_ERROR
    "Error forking process.\n",
    // MSG_FORK_WAIT_ERROR
    "Error while waiting for exit of child process.\n",
    // MSG_OPEN_ERROR
    "Error while opening file.\n",
    // MSG_THREAD_CREATE_ERROR
    "Error while creating thread\n",
    // MSG_THREAD_JOIN_ERROR
    "Error while joining the thread\n"
};

/**
 * @brief Function prints input string and flushes stdout
 * @param s input string
 */
void printAndFlush(int i) {
    if (i < 2) {
        printf("%s", msgs[i]);
        fflush(stdout);
    } else {
        fprintf(stderr, "%s", msgs[i]);
        fflush(stderr);
    }
}

/**
 * @brief Function handles SIGCHLD signal
 * @param s signal ID
 */
static void sigchildHandler(int s) {
    pid_t p;
    while ((p = waitpid(-1, NULL, WNOHANG)) > 0)
        if (s == SIGCHLD) {
            printf("\n+ [%d] done\n$ ", p);
            fflush(stdout);
        }
}

/**
 * @brief Function handles SIGINT signal
 * @param s signal ID
 */
static void sigintHandler(int s) {
    if (s == SIGINT) printAndFlush((!kill(childPid, SIGINT))? MSG_NEWLINE : MSG_NEW_PROMPT_LINE);
}

/**
 * @brief Function handles interrupt signal (SIGINT) in in shell
 * @param s signal ID
 */
static void sigintShellHandler(int s) {
    if (s == SIGINT) printAndFlush(MSG_NEW_PROMPT_LINE);
}

/**
 * @brief Function finds given char in token
 * @param c given char to find in token
 * @param str given string token
 * @return index of str in token or -1 if c is not in token
 */
int findCharInString(char c, char *str) {
    const char *ptr = strchr(str, c);
    return (int) ((ptr) ? ptr - str : (unsigned long) -1);
}

/**
 * @brief Function trims leading and trailing whitespaces in string
 * @param s input string
 */
 void trim(char *s) {
    char *start, *end;
    // find first non whitespace character
    for (start = s; *start; start++)
        if (!isspace((unsigned char) start[-1])) break;
    // find start of whitespace tail
    for (end = start + strlen(start); end > start + 1; end--)
        if (!isspace((unsigned char) end[-1])) break;
    // truncate last whitespace
    *end = 0;
    if (start > s) memmove(s, start, (end - start) + 1);
}

/**
 * @brief Function sets cmdFlags
 * @param s string
 */
void setCmdFlags(char *s) {
    if (findCharInString(REDIRECT_STDOUT_SYMBOL, s) > -1)cmdFlags[REDIRECT_STDOUT_FLAG] = true;
    if (findCharInString(REDIRECT_STDIN_SYMBOL, s) > -1) cmdFlags[REDIRECT_STDIN_FLAG] = true;
    if (findCharInString(BACKGROUND_PROCESS_SYMBOL, s) > -1) cmdFlags[BACKGROUND_PROCESS_FLAG] = true;
}

/**
 * @brief Function clears command flags
 */
void clearFlags(void) {
    tooLongInput = false;
    cmdFlags[REDIRECT_STDOUT_FLAG] = false;
    cmdFlags[REDIRECT_STDIN_FLAG] = false;
    cmdFlags[BACKGROUND_PROCESS_FLAG] = false;
}

/**
 * @brief Function parses cmdline arguments loaded in buffer and returns argument vector
 * @return argv
 */
char **parseArguments(void) {
    // if too long input then print error and read next input
    if (tooLongInput) {
        printAndFlush(MSG_TOO_LONG_INPUT);
        return NULL;
    }
    // argv's size is set by default to estimated value
    int argc = 0, argvAllocSize = ESTIMATED_ARGV_SIZE;
    char **argv = NULL;
    // remove leading and trailing whitespaces
    trim(buffer);
    // while there are tokens, put them to argument vector
    for (char *token = strtok(buffer, DELIMITERS); token; token = strtok(NULL, DELIMITERS)) {
        // allocate memory for argv
        if (!argc) {
            argv = (char **) malloc(sizeof(char *) * argvAllocSize);
            // if input buffer contains equal amount of tokens as the estimated allocated space
            // argv is reallocated space is extended by another estimated size
            //  [X][X][X][X][X][X][_][_][_][_][_][_]
            //  | est. argv size ||  reallocation  |
        } else if (argc == argvAllocSize-1) {
            argvAllocSize += ESTIMATED_ARGV_SIZE;
            argv = (char **) realloc(argv, sizeof(char *) * argvAllocSize);
        }
        // set cmd flags
        setCmdFlags(token);
        argv[argc] = token;
        // the last token must be NULL because of exec() system call
        argv[++argc] = NULL;
    }
    // get file paths and update argv
    if (argv) {
        if ((cmdFlags[REDIRECT_STDOUT_FLAG] && !(outputFileName = getFilePath(REDIRECT_STDOUT_SYMBOL, argv))) ||
            (cmdFlags[REDIRECT_STDIN_FLAG] && !(inputFileName = getFilePath(REDIRECT_STDIN_SYMBOL, argv)))) {
            printAndFlush(MSG_INVALID_INPUT);
            return NULL;
        }
        if (cmdFlags[BACKGROUND_PROCESS_FLAG]) {
            if (argc == 1) {
                printAndFlush(MSG_INVALID_INPUT);
                return NULL;
            } else {
                removeBackgroundProcessSymbol(argv);
            }
        }
    }
    // test for shell exit
    if (argc == 1 && argv[0] && !strcmp(argv[0], "exit") && !argv[1]) exitShell = true;
    return argv;
}

/**
 * @brief Function returns file path for given command symbol from argv
 * @param cmdSymbol <, >
 * @param argv argument vector
 * @return file path or NULL
 */
char *getFilePath(char cmdSymbol, char **argv) {
    bool nextArgvItemIsFilePath = false;
    char *filePath = NULL;
    for (int i = 0, charPos = -1; argv[i]; i++) {
        // if previous argv item was > or < this argv item is the file path
        if (nextArgvItemIsFilePath) {
            filePath = argv[i];
            // have to remove file path from argv to be able to pass argv to exec()
            argv[i] = NULL;
            break;
        }
        // if cmdSymbol in argv item
        if ((charPos = findCharInString(cmdSymbol, argv[i])) > -1) {
            // case [.., ">", "filePath", ..]
            if (strlen(argv[i]) == 1) {
                nextArgvItemIsFilePath = true;
                argv[i] = NULL;
            // case [.., ">filePath", ..]
            } else if (charPos == 0) {
                filePath = argv[i] + 1;
                argv[i] = NULL;
            // case [.., "ls>", "/tmp/test", ..]
            } else if (strlen(argv[i])-1 == charPos) {
                nextArgvItemIsFilePath = true;
                argv[i][charPos] = TRAILING_ZERO_SYMBOL;
            // or case [.., "ls>/tmp/test", ..]
            } else {
                filePath = argv[i] + charPos + 1;
                argv[i][charPos] = TRAILING_ZERO_SYMBOL;
            }
        }
    }
    return filePath;
}

/**
 * @brief Function removed the background process symbol (&)
 * @param argv argument vector
 */
void removeBackgroundProcessSymbol(char **argv) {
    for (int i = 0, charPos = -1; argv[i]; i++) {
        if ((charPos = findCharInString(BACKGROUND_PROCESS_SYMBOL, argv[i])) > -1) {
            // case ["sleep", "3s", "&"]
            if (charPos == 0) argv[i] = NULL;
            // case ["sleep", "3s&"]
            else argv[i][charPos] = TRAILING_ZERO_SYMBOL;
        }
    }
}

/**
 * @brief Function reads input from user and fills the buffer
 * @param t thread parameters
 * @return
 */
void *readRoutine(void *t) {
    // while not done
    while (!exitShell) {
        pthread_mutex_lock(&mtx);
        ssize_t buffSize, tmpBuffSize;
        // print shell prompt
        printAndFlush(MSG_NEW_PROMPT_LINE);
        // initialize buffer and read user input
        memset(buffer, TRAILING_ZERO_SYMBOL, MAX_BUFFER_SIZE);
        for (buffSize = 0; buffer[buffSize-1] != NEWLINE_SYMBOL;) {
            tmpBuffSize = read(fileno(stdin), &buffer, MAX_BUFFER_SIZE);
            buffSize += tmpBuffSize;
            if (buffSize > MAX_BUFFER_SIZE - 1) tooLongInput = true;
        }
        isReading = true;
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mtx);
        // wait for running thread
        pthread_mutex_lock(&mtx);
        while (isReading) pthread_cond_wait(&cond, &mtx);
        pthread_mutex_unlock(&mtx);
    }
    pthread_exit(NULL);
}

/**
 * @brief Function executes command from input buffer
 * @param t thread params
 */
void *runRoutine(void *t) {
    int child, status;
    pid_t pid;
    char **argv;
    struct sigaction s_child, s_int;
    // while not entered exit command
    while(!exitShell) {
        // wait for reading thread
        pthread_mutex_lock(&mtx);
        // condition wait
        while (!isReading) pthread_cond_wait(&cond, &mtx);
        // parse arguments and return argument vector (char **)
        argv = parseArguments();
        // if there is some input
        if (argv && !exitShell) {
            // get input/output file names from argv
            child = fork();
            // parent code
            if (child == 0) {
                if (cmdFlags[REDIRECT_STDOUT_FLAG]) redirect(REDIRECT_STDOUT_SYMBOL, outputFileName);
                if (cmdFlags[REDIRECT_STDIN_FLAG]) redirect(REDIRECT_STDIN_SYMBOL, inputFileName);
                if (cmdFlags[BACKGROUND_PROCESS_FLAG]) {
                    setsid();
                    ignoreSignals();
                }
                // execute command specified in input buffer (argv)
                if (execvp(*argv, argv) == -1) {
                    fprintf(stderr, "%s\n", strerror(errno));
                    fflush(stderr);
                }
                // exit this process
                _exit(0);
            // child code
            } else if(child > 0) {
                // if background command
                if (cmdFlags[BACKGROUND_PROCESS_FLAG]) {
                    memset(&s_child, 0, sizeof(s_child));
                    s_child.sa_handler = sigchildHandler;
                    sigaction(SIGCHLD, &s_child, NULL);
                    printf("[%d] %s\n", child, *argv);
                    fflush(stdout);
                // if not background command
                } else {
                    childPid = child;
                    memset(&s_int, 0, sizeof(s_int));
                    s_int.sa_handler = sigintHandler;
                    sigaction(SIGINT, &s_int, NULL);
                    pid = wait(&status);
                    if (pid == -1) printAndFlush(MSG_FORK_WAIT_ERROR);
                }
            // child < 0 - error
            } else printAndFlush(MSG_FORK_ERROR);
            // clear flags & free memory
            free(argv);
        // if argv contains nothing
        }
        clearFlags();
        inputFileName = NULL;
        outputFileName = NULL;
        isReading = false;
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mtx);
    }
    pthread_exit(NULL);
}

/**
 * @brief Function redirects stdin/stdout to file
 * @param c one of ['>', '<']
 * @param fileName destination of redirection file path
 */
void redirect(char c, char *fileName) {
    int fd = -1;
    // >
    if (c == REDIRECT_STDOUT_SYMBOL) {
        fd = open(fileName, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
        if (fd < 0) printAndFlush(MSG_OPEN_ERROR);
        close(fileno(stdout));
    // <
    } else {
        fd = open(fileName, O_RDONLY);
        if (fd < 0) printAndFlush(MSG_OPEN_ERROR);
        close(fileno(stdin));
    }
    // duplicate file descriptor
    dup(fd);
    close(fd);
}

/**
 * @brief Function sets several signals to ignore while background process active
 */
void ignoreSignals(void) {
    // if child process dies
    signal(SIGCHLD, SIG_DFL);
    // terminate signal
    signal(SIGTERM, SIG_DFL);
    // interrupt signal
    signal(SIGINT, SIG_IGN);
    // interactive stop signal
    signal(SIGTSTP, SIG_IGN);
    // background process is unable to write to stdout
    signal(SIGTTOU, SIG_IGN);
    // background process is unable to read from stdin
    signal(SIGTTIN, SIG_IGN);
    // hangup signal
    signal(SIGHUP, SIG_IGN);
}

/**
 * @brief Function deallocates mutex and condition
 */
void freeThreadSources(void) {
    pthread_mutex_destroy(&mtx);
    pthread_cond_destroy(&cond);
}

/**
 * @brief Main function
 */
int main(void) { ;
    pthread_t readingThread, runningThread;
    struct sigaction s_shell;
    // handle interrupt signal in shell - will be ignored
    memset(&s_shell, 0, sizeof(s_shell));
    s_shell.sa_handler = sigintShellHandler;
    sigaction(SIGINT, &s_shell, NULL);
    // create reading and running thread
    if (pthread_create(&(readingThread), NULL, readRoutine, NULL) != 0 ||
        pthread_create(&(runningThread), NULL, runRoutine, NULL) != 0) {
        printAndFlush(MSG_THREAD_CREATE_ERROR);
        freeThreadSources();
        return 1;
    }
    // join the 2 threads
    if (pthread_join(readingThread, NULL) != 0 ||
        pthread_join(runningThread, NULL) != 0) {
        printAndFlush(MSG_THREAD_JOIN_ERROR);
        freeThreadSources();
        return 2;
    }
    return 0;
}
